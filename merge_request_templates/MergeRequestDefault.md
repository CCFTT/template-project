*Feature|Chore|Bug* :memo:  
* [O que foi feito 1];  
* [O que foi feito 2];  
* [O que foi feito 3]; 

*Tarefa* :credit_card:  
* [Link tarefa]

*Code Review* :clipboard: 

- Código
    -   [ ] O recurso funciona?
    -   [ ] Desempenha o papel esperado, a lógica está correta, cumpre o requerido na task?
    -   [ ] É facilmente entendido?
    -   [ ] Respeita as convenções de codificação definidas para o projeto?
    -   [ ] Existe algum código redundante ou duplicado?
    -   [ ] É o mais modular possível?
    -   [ ] Algum código de log ou debug pode ser removido?
    -   [ ] Foi removido todo código comentado?
	-   [ ] Alguma variável global pode ser substituída?
	-   [ ] Existe algum código comentado?
	-   [ ] Os laços de repetição têm a condição de parada correta?
	-   [ ] Os nomes usados transmitem intenção? - 
-   Segurança
    -   [ ] Todos os inputs foram validados
    -   [ ] Tipo
    -   [ ] Maxlenght
    -   [ ] Formato
    -   [ ] Valores válidos
    -   [ ] Os parâmetros inválidos foram tratados?
-   Documentação
    -   [ ] É necessário executar algum comando extra para que o código funcione?
    -   [ ] O código possui documentação? Nos principais métodos e lógicas complexas?
    -   [ ] Todas as variáveis foram definidas com:
	    -   [ ] Nomes significativos
	    -   [ ] Consistentes
	    -   [ ] Legíveis
	    -   [ ] Claro  
	    -  [ ] Os comentários existem e descreve a intenção do código?
	    -   [ ] O uso e a função de bibliotecas de terceiros estão documentados?
	    -   [ ] Existe algum código incompleto? Nesse caso, ele deve ser removido ou sinalizado com um marcador adequado como "TODO"?  
-   Performance
    -   [ ] As consultas do Doctrine (ou do banco de dados, ou Zend\Sql, etc) foram otimizadas pensando-se em melhoria de performance?
    -   [ ] Informações que podem ser armazenadas em cache estão sendo cacheadas?
    -   [ ] Processamentos redundantes ou lentos foram otimizados?
    -   [ ] Foi evitado o uso de construções IF-ELSE para diminuir a complexidade da execução?
    -   [ ] Existem otimizações óbvias que melhorarão o desempenho?
    -   [ ] Algum do código pode ser substituído por biblioteca ou funções nativas?
-   Banco de dados
    -   [ ] Nome da tabela está correto.
    -   [ ] Estrutura da tabela está correta:
    -   [ ] Descrição dos campos.
    -   [ ] Tipo.
    -   [ ] Restrições e valores default.
    -   [ ] Charset.
    -   [ ] Charset do banco de dados.
    -   [ ] As migrations foram bem criadas?
    -   [ ] Arquivo de update.
-   Testes de navegadores
	-   Testar a interface seguindo os requisitos mínimos de navegadores:
	    -   [ ] Microsoft Edge
	    -   [ ] Safari
	    -   [ ] Chrome 
	    -   [ ] Firefox version 
- Teste
    -   [ ] O código é testável? O código deve ser estruturado para não adicionar muitos ou ocultar dependências, não conseguir inicializar objetos, as estruturas de teste podem usar métodos etc.
    -   [ ] Os testes existem e são compreensivos?
    -   [ ] Os teste unitários testam realmente a funcionalidade desenvolvida?
    -   [ ] Qualquer teste pode ser substituído pelo uso de uma API existente?
